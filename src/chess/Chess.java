package chess;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import chesspieces.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * The Chess class holds the main method and all the chess board logic
 */
public class Chess {
	/**
	 * Printed when a move is invalid
	 */
	private final static String invalid = "Illegal move, try again";
	/**
	 * Used to make the white player go first
	 */
	private static boolean whitesTurn = true;
	/**
	 * Array of file strings for convenience
	 */
	private static String files[] = {"a","b","c","d","e","f","g","h"};
	/**
	 * Stores the location of the black king
	 */
	private static String blackKingLocation = "e8";
	/**
	 * Stores the location of the white king
	 */
	private static String whiteKingLocation = "e1";
	/**
	 * Main data structure for storing the chess pieces on the board
	 */
	private static Map<String,ChessPiece> board = new HashMap<String,ChessPiece>();
	/**
	 * Boolean used to determine if a draw is made
	 */
	private static boolean offerDraw = false;
	
	/**
	 * Boolean used to determine if the white king moved for castling logic
	 */
	static boolean whiteKingMoved = false;
	/**
	 * Boolean used to determine if the black king moved for castling logic
	 */
	static boolean blackKingMoved = false;
	/**
	 * Boolean used to determine if the left white rook moved for castling logic
	 */
	static boolean leftWhiteRookMoved = false;
	/**
	 * Boolean used to determine if the right white rook moved for castling logic
	 */
	static boolean rightWhiteRookMoved = false;
	/**
	 * Boolean used to determine if the left black rook moved for castling logic
	 */
	static boolean leftBlackRookMoved = false;
	/**
	 * Boolean used to determine if the right black rook moved for castling logic
	 */
	static boolean rightBlackRookMoved = false;
	
	/**
	 * Boolean used to determine if the current player is in check
	 */
	static boolean check = false;
	
	/**
	 * String used to store the previous move that was made
	 */
	static String prevMove = "";
	
	/**
	 * Main method used to run the game of chess
	 * @param args no args needed
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		initializeBoard();
		
		startGame();
		
		
		//debug();
		/* used to easily test if the moving pieces logic is working. only white moves.
		* will delete before submission.
		*/
		/*Set<String> moves = getAllPlayerMoves(board.keySet(), 'w');
		for(String key : moves) {
			System.out.println(key);
		} */
	}
	
	/**
	 * initializes the standard starting board for chess
	 */
	private static void initializeBoard() {
		board.put("a8", new Rook("b","a8"));
		board.put("b8", new Knight("b","b8"));
		board.put("c8", new Bishop("b","c8"));
		board.put("d8", new Queen("b","d8"));
		board.put("e8", new King("b","e8"));
		board.put("f8", new Bishop("b","f8"));
		board.put("g8", new Knight("b","g8"));
		board.put("h8", new Rook("b","h8"));
		
		for (String file: files) {
			board.put(file+7+"", new Pawn("b",file+7+""));
			board.put(file+2+"", new Pawn("w",file+2+""));
		}
		
		board.put("a1", new Rook("w","a1"));
		board.put("b1", new Knight("w","b1"));
		board.put("c1", new Bishop("w","c1"));
		board.put("d1", new Queen("w","d1"));
		board.put("e1", new King("w","e1"));
		board.put("f1", new Bishop("w","f1"));
		board.put("g1", new Knight("w","g1"));
		board.put("h1", new Rook("w","h1"));
	}
	
	/**
	 * prints the current state of the board
	 */
	private static void printBoard() {
		String key;
		for (int rank = 8; rank > 0 ; rank--) {
			for (int file = 0; file < 8; file++) {
				key = files[file]+rank+"";
				if (board.containsKey(key)) {
					System.out.print(board.get(key)+" ");
				} else {
					if ((rank % 2 == 0 && file % 2 == 0) || (rank % 2 == 1 && file % 2 == 1))
						System.out.print("   ");
					else
						System.out.print("## ");
				}
			}
			System.out.print(rank+"\n");
		}
		for (String file: files) {
			System.out.print(" " + file + " ");
		}
		System.out.println();
		System.out.println();
		
	}
	
	 /** Loops through a game of chess until it has ended.
	 * @throws IOException
	 */
	private static void startGame() throws IOException {
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 boolean gameOver = false;
		 String lastMove;
		 Set<String> moves = new HashSet<String>();
		 Set<String> currentPlayerMoves = new HashSet<String>();
		 boolean checkMate = false;
		 while(!gameOver) {
			 printBoard();
			 if(checkMate == true) {
				 System.out.println("CheckMate");
				 if(whitesTurn) {
					 System.out.println("Black Wins");
					 break;
				 } else {
					 System.out.println("White Wins");
					 break;
				 }
			 } else if (check == true) {
				 System.out.println("Check");
			 }	else if (check == false) {
				 // A stalemate occurs if the player is not in check and there are no valid moves
				 if ((whitesTurn && getAllPlayerMoves(board.keySet(), 'w').isEmpty()) || 
						 (!whitesTurn && getAllPlayerMoves(board.keySet(), 'b').isEmpty())) {
					 System.out.println("Stalemate");
					 break;
				 }
			 }
			 if(whitesTurn) {
				 whitesTurn = false;
				 lastMove =playTurn("w", br);
				 check = false;
				 checkMate = false;
				 prevMove = lastMove;
				 if(lastMove.equals("resign")) {
					 System.out.println("Black wins");
					 break;
				 }
				 if(lastMove.equals("draw")) break;
				 if(lastMove.equals(blackKingLocation)) {
					 System.out.println("White wins");
					 gameOver = true;
				 } else {
					 moves = getAllPlayerMoves(board.keySet(), 'w');
					 currentPlayerMoves = getAllPlayerMoves(board.keySet(), 'b');
				 	 if(moves.contains(blackKingLocation)) { // check if the last move put the enemy in check
				 		 check = true;
				 		  Set<String> kingsMoves = getValidMoves(blackKingLocation, board.get(blackKingLocation),'b');
				 		  checkMate = true;
				 		 for(String key : currentPlayerMoves) {
				 			  if(moves.contains(key)) {
				 				  moves.remove(key);
				 			  }
				 		  }
				 		 for(String key : kingsMoves) {
				 			 if(!moves.contains(key)) {
				 				 checkMate = false;
				 				 break;
				 			 }
				 		 }
				 	 }
				 }
			 } else {
				 whitesTurn = true;
				 lastMove = playTurn("b", br);
				 check = false;
				 checkMate = false;
				 prevMove = lastMove;
				 if(lastMove.equals("resign")) {
					 System.out.println("White wins");
					 break;
				 }
				 if(lastMove.equals("draw")) break;
				 if(lastMove.equals(whiteKingLocation)) {
					 System.out.println("Black wins");
					 gameOver = true;
				 } else {
					 moves = getAllPlayerMoves(board.keySet(), 'b');
					 currentPlayerMoves = getAllPlayerMoves(board.keySet(), 'w');
				 	 if(moves.contains(whiteKingLocation)) { // check if the last move put the enemy in check
				 		 check = true;
				 		  Set<String> kingsMoves = getValidMoves(whiteKingLocation, board.get(whiteKingLocation),'w');
				 		  checkMate = true;
				 		  for(String key : currentPlayerMoves) {
				 			  if(moves.contains(key)) {
				 				  moves.remove(key);
				 			  }
				 		  }
				 		 for(String key : kingsMoves) {
				 			 if(!moves.contains(key)) {
				 				 checkMate = false;
				 				 break;
				 			 }
				 		 }
				 	 }
				 }
			 }
		 }
		 br.close();
	 } 
	 /** asks the current player for input until a valid input is given .
	 * @param color current color either white or black
	 * @param br the buffer reader for reading input.
	 * @return a String representing the space the piece has moved to, "draw" representing the game has
	 *  ended in a draw, or "resign" representing the game has ended in a draw.
	 * @throws IOException
	 */
	private static String playTurn(String color, BufferedReader br) throws IOException {
		 boolean validMove = false;
		 while(!validMove) {
			 if(color.equals("w")) {
				 System.out.print("White's move: ");
			 } else {
				 System.out.print("Black's move: ");
			 }
			 String s = br.readLine();
			 
			 if(s.equals("resign"))return "resign";
			 
			 if(offerDraw == true) {
				 if(s.equals("draw")) {
					 System.out.println("draw");
					 return "draw";
				 }
			 }
			 offerDraw = false;
			 if(s.indexOf("draw?") != -1) {
				 offerDraw = true;
				 //s = s.substring(0,5);
			 }
			 System.out.println();
			 String cur = s.substring(0,2);
		 	 String dest = s.substring(3,5);
		 	if(s.indexOf("e1 g1") != -1 || s.indexOf("e1 c1") != -1 || s.indexOf("e8 g8") != -1 || s.indexOf("e8 c8") != -1) {
				 if(checkCastle( cur, dest,color.charAt(0))) {
					 if(color.charAt(0) == 'w') return whiteKingLocation;
					 else return blackKingLocation;
				 } else {
					 System.out.println(invalid);
					 System.out.println();
					 continue;
				 }
			 }
		 	 if(!board.containsKey(cur)) { // if there is no piece in that spot
		 		 System.out.println(invalid);
		 		 System.out.println();
		 	 } else if (!board.get(cur).getColor().equals(color)) { // if the user tries to move the opposite color piece
		 		 System.out.println(invalid);
		 		 System.out.println();
		 	 } else {
		 		 ChessPiece curPiece = board.get(cur);
		 		 Set<String> validMoves = getValidMoves(cur, curPiece, color.charAt(0));
		 		 if(validMoves.contains(dest)) {
		 			 validMove = true;
		 			 
		 			 
		 			 if(check == true) { 
			 			 ChessPiece chessPiece = null;
			 			 if(board.containsKey(dest)) chessPiece =  board.get(dest);
			 			 board.put(dest, curPiece);
			 			 board.remove(cur);
			 			 String tempWhiteKingLocation = whiteKingLocation;
			 			 String tempBlackKingLocation = blackKingLocation;
			 			if(color.charAt(0) == 'w' && cur.equals(whiteKingLocation)) {
			 				tempWhiteKingLocation = dest;
			 			} else if(color.charAt(0) == 'b' && cur.equals(blackKingLocation) ) {
			 				tempBlackKingLocation = dest;
			 			}
			 			 Set<String> opponentMoves;
			 			 if(color.charAt(0) == 'w') opponentMoves = getAllPlayerMoves(board.keySet(), 'b' );
			 			 else opponentMoves = getAllPlayerMoves(board.keySet(), 'w' );
			 			
			 			 board.put(cur, board.get(dest));
			 			 if(chessPiece != null) {
			 				 board.put(dest, chessPiece);
			 			 } else {
			 				 board.remove(dest);
			 			 }
			 			 
		 				 if(color.equals("w")) {
		 					 if(opponentMoves.contains(tempWhiteKingLocation)) {
		 						System.out.println(invalid);
		 				 		System.out.println();
		 				 		validMove = false;
		 				 		continue;
		 					 }
		 				 } else {
		 					if(opponentMoves.contains(tempBlackKingLocation)) {
		 						System.out.println(invalid);
		 				 		System.out.println();
		 				 		validMove = false;
		 				 		continue;
		 					 }
		 				 }
		 			 }
		 			 
		 			 if(curPiece.toString().charAt(1) == 'P') {
		 				 if(dest.charAt(0) != cur.charAt(0) && !board.containsKey(dest)) { // en passant check
		 					 char file = dest.charAt(0);
		 					 int rank = Integer.parseInt(dest.substring(1));
		 					 if(color.equals("w")) {
		 						 board.remove((char) (file) + Integer.toString(rank-1));
		 					 } else {
		 						board.remove((char) (file) + Integer.toString(rank+1));
		 					 }
		 				 }
		 			 }
		 			 if(cur.equals("a1") || dest.equals("a1")) leftWhiteRookMoved = true;
		 			 if(cur.equals("h1") || dest.equals("h1")) rightWhiteRookMoved = true;
		 			 if(cur.equals("a8") || dest.equals("a8")) rightBlackRookMoved = true;
		 			 if(cur.equals("h8") || dest.equals("h8")) leftBlackRookMoved = true;
		 			 if(cur.equals("e1")) whiteKingMoved = true;
		 			 if(cur.equals("e8")) blackKingMoved = true;
		 			 curPiece.setLocation(dest);
		 			 curPiece = promote(curPiece, s); //checks if this is a pawn and if it is promote it to queen
		 			 board.put(dest, curPiece);
		 			 board.remove(cur);
		 			 
		 			 if(curPiece.toString().charAt(1) == 'K') { //if the king moved changed update 
		 				 if(curPiece.getColor() == "w") {
		 					 whiteKingLocation = dest;
		 				 } else {
		 					 blackKingLocation = dest;
		 				 }
		 			 }
		 			 return dest;
		 		 } else {
		 			 System.out.println(invalid);
		 			 System.out.println();
		 		 }
		 	 }
		 }
		 return null; 
	 }
	 /** method to sort out which piece we are dealing with and call the appropriate method to find valid moves.
	 * @param cur the current space the piece is on
	 * @param chessPiece the current chess piece we are evaluating.
	 * @param color current color
	 * @return the set of strings representing valid moves
	 */
	public static Set<String> getValidMoves(String cur, ChessPiece chessPiece, char color){
		 char piece = chessPiece.toString().charAt(1);
		 Set<String> validMoves = new HashSet<String>();
		 char file = cur.charAt(0);
		 int rank = Integer.parseInt(cur.substring(1));
		 if(piece == 'P') {
			 getPawnMoves(validMoves, color, file, rank);
		 } else if(piece == 'N') {
			 getKnightMoves(validMoves, color, file, rank);
		 } else if(piece == 'R') {
			 getRookMoves(validMoves, color, file, rank);
		 } else if(piece == 'B') {
			 getBishopMoves(validMoves, color, file, rank);
		 } else if(piece == 'Q') { // since queen has the same moves as Bishop + Rook
			 getBishopMoves(validMoves, color, file, rank);
			 getRookMoves(validMoves, color, file, rank);
		 } else {
			 getKingMoves(validMoves, color, file, rank);
		 }
		 return validMoves;
	 }

	 /** Adds the space to validSpace if either there is nothing in that current space or 
	  * the space is occupied by the enemy player's piece.
	 * @param validMoves the Set of valid moves that the move will be added to if valid.
	 * @param updated the move to check if it is valid
	 * @param color current color
	 * @return true if the space was empty, false otherwise
	 */
	private static boolean checkAndAdd(Set<String> validMoves , String updated, char color ) {
		 if(board.containsKey(updated)) {
				if(board.get(updated).getColor().charAt(0) != color) {
					validMoves.add(updated);
				}
				return false;
		  } else {
			  validMoves.add(updated);
			  return true;
		  }
	 }
	 
	 /** Checks if the input ChessPiece is a pawn and deserves to be promoted based on it's location
	 * @param cur the ChessPiece we are checking
	 * @return either a new Queen chessPiece if the pawn is promoted or the same ChessPiece that was inputed
	 */
	private static ChessPiece promote (ChessPiece cur, String input) {
		
		 if(cur.toString().charAt(1) == 'P') {
			 if(cur.toString().charAt(0) == 'b' && cur.getLocation().toString().charAt(1) == '1') {
				 if(input.length() < 7) {
					 cur = new Queen("b", cur.getLocation());
				 } else {
					 char type = input.charAt(6);
					 if(type == 'N') {
						 cur = new Knight("b", cur.getLocation());
					 } else if(type == 'R') {
						 cur = new Rook("b", cur.getLocation());
					 } else if(type == 'B') {
						 cur = new Bishop("b", cur.getLocation());
					 } else {
						 cur = new Queen("b", cur.getLocation());
					 }
				 }
				 
			 } else if(cur.toString().charAt(0) == 'w' && cur.getLocation().toString().charAt(1) == '8') {
				 cur = new Queen("w", cur.getLocation());
				 if(input.length() < 7) {
					 cur = new Queen("w", cur.getLocation());
				 } else {
					 char type = input.charAt(6);
					 if(type == 'N') {
						 cur = new Knight("w", cur.getLocation());
					 } else if(type == 'R') {
						 cur = new Rook("w", cur.getLocation());
					 } else if(type == 'B') {
						 cur = new Bishop("w", cur.getLocation());
					 } else {
						 cur = new Queen("w", cur.getLocation());
					 }
				 }
			 }
		 }
		 return cur;
	 }
	 
	 /** Adds all of the valid moves for the Pawn piece to the validMoves Set.
	 * @param validMoves the Set where the valid moves will be added to
	 * @param color current color
	 * @param file current file
	 * @param rank current rank
	 */
	private static void getPawnMoves(Set<String> validMoves, char color, char file, int rank) {
		 String updated = "";
		 boolean hasMoved = true; // if this is the first time pawn is moving it can move 2 spaces
		 if(color == 'w') {
			 if(rank == 2)hasMoved = false;
			 rank++;
		 } else {
			 if(rank == 7)hasMoved = false;
			 rank--;
		 }
		 updated += file + Integer.toString(rank);
		 
		 
		 if(!board.containsKey(updated) && checkAndAdd(validMoves, updated, color) && hasMoved == false){ // pawns can move twice there first turn
			 if(color == 'w') {
				 rank++;
			 } else {
				 rank--;
			 }
			 updated = file + Integer.toString(rank);
			 if (!board.containsKey(updated)) checkAndAdd(validMoves, updated, color);
			 if(color == 'w') {
				 rank--;
			 } else {
				 rank++;
			 }
			 
		 }
		 if(color == 'w') { // reset from adding it earlier
			 rank--;
		 } else {
			 rank++;
		 }
		 
		 if(color == 'w') {
			 if(rank + 1 < 9 && file -1 >= 'a' && board.containsKey((char) (file -1) + Integer.toString(rank+1)) ) checkAndAdd(validMoves, (char) (file -1) + Integer.toString(rank+1), 'w');
			 if(rank + 1 < 9 && file +1 <= 'h' && board.containsKey((char) (file +1) + Integer.toString(rank+1))) checkAndAdd(validMoves, (char) (file +1) + Integer.toString(rank+1), 'w');
			 if(rank + 1 < 9 && file -1 >= 'a' && !board.containsKey((char) (file -1) + Integer.toString(rank+1))) {
				 if(prevMove.equals((char) (file -1) + Integer.toString(rank)) && rank + 2 == 7 && board.get((char) (file -1) + Integer.toString(rank)).toString().charAt(1) == 'P') {
					 validMoves.add((char) (file -1) + Integer.toString(rank+1));
				 }
			 }
			 if(rank + 1 < 9 && file +1 <= 'h' && !board.containsKey((char) (file +1) + Integer.toString(rank+1))) {
				 if(prevMove.equals((char) (file +1) + Integer.toString(rank)) && rank + 2 == 7 && board.get((char) (file +1) + Integer.toString(rank)).toString().charAt(1) == 'P') {
					 validMoves.add((char) (file +1) + Integer.toString(rank+1));
				 }
			 }
			 
		 } else {
			 if(rank - 1 > 0 && file +1 <= 'h' && board.containsKey((char) (file +1) + Integer.toString(rank-1))) checkAndAdd(validMoves, (char) (file +1) + Integer.toString(rank-1), 'b');
			 if(rank - 1 > 0 && file -1 <= 'h' && board.containsKey((char) (file -1) + Integer.toString(rank-1))) checkAndAdd(validMoves, (char) (file -1) + Integer.toString(rank-1), 'b');
			 if(rank - 1 > 0 && file -1 >= 'a' && !board.containsKey((char) (file -1) + Integer.toString(rank-1))) {
				 if(prevMove.equals((char) (file -1) + Integer.toString(rank)) && rank + 2 == 7 && board.get((char) (file -1) + Integer.toString(rank)).toString().charAt(1) == 'P') {
					 validMoves.add((char) (file -1) + Integer.toString(rank-1));
				 }
			 }
			 if(rank - 1 > 0 && file +1 <= 'h' && !board.containsKey((char) (file +1) + Integer.toString(rank-1))) {
				 if(prevMove.equals((char) (file +1) + Integer.toString(rank)) && rank - 2 == 2 && board.get((char) (file +1) + Integer.toString(rank)).toString().charAt(1) == 'P') {
					 validMoves.add((char) (file +1) + Integer.toString(rank-1));
				 }
			 }
		 }
		 
		 
		 
		 
	 }
	 
	 /** Adds all of the valid moves for the Knight piece to the validMoves Set.
	 * @param validMoves the Set where the valid moves will be added to
	 * @param color current color
	 * @param file current file
	 * @param rank current rank
	 */
	private static void getKnightMoves(Set<String> validMoves, char color, char file, int rank) {
		 List<String> movesToCheck = new ArrayList<String>();
		 
		 if(rank - 2 > 0 && file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank-2));
		 if(rank - 1 > 0 && file -2 >= 'a') movesToCheck.add((char) (file -2) + Integer.toString(rank-1));
		 if(rank - 2 > 0 && file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank-2));
		 if(rank + 2 < 9 && file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank+2));
		 if(rank + 1 < 9 && file -2 >= 'a') movesToCheck.add((char) (file -2) + Integer.toString(rank+1));
		 if(rank - 1 > 0 && file +2 <= 'h') movesToCheck.add((char) (file +2) + Integer.toString(rank-1));
		 if(rank + 1 < 9 && file +2 <= 'h') movesToCheck.add((char) (file +2) + Integer.toString(rank+1));
		 if(rank + 2 < 9 && file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank+2));
		 //now check if these moves are valid
		 for(int i =0; i < movesToCheck.size(); i ++) {
			 checkAndAdd(validMoves, movesToCheck.get(i), color);
		 }
		 
	 }
	 
	 /** Adds all of the valid moves for the Rook piece to the validMoves Set.
	 * @param validMoves the Set where the valid moves will be added to
	 * @param color current color
	 * @param file current file
	 * @param rank current rank
	 */
	private static void getRookMoves(Set<String> validMoves, char color, char file, int rank) {
		 List<String> movesToCheck = new ArrayList<String>();
		 int tempRank = rank;
		 char tempFile = file;
		 tempRank--;
		 while(tempRank > 0) {
			 if(board.containsKey((char)file + Integer.toString(tempRank))) {
				 movesToCheck.add((char)file + Integer.toString(tempRank));
				 break;
			 } else {
				 movesToCheck.add((char)file + Integer.toString(tempRank));
			 }
			 tempRank--;
		 }
		 tempRank = rank;
		 tempRank++;
		 while(tempRank < 9) {
			 if(board.containsKey((char)file + Integer.toString(tempRank))) {
				 movesToCheck.add((char)file + Integer.toString(tempRank));
				 break;
			 } else {
				 movesToCheck.add((char)file + Integer.toString(tempRank));
			 }
			 tempRank++;
		 }
		 tempFile--;
		 while(tempFile >= 'a') {
			 if(board.containsKey((char)tempFile + Integer.toString(rank))) {
				 movesToCheck.add((char)tempFile + Integer.toString(rank));
				 break;
			 } else {
				 movesToCheck.add((char)tempFile + Integer.toString(rank));
			 }
			 tempFile--;
		 }
		 tempFile = file;
		 tempFile++;
		 while(tempFile <= 'h') {
			 if(board.containsKey((char)tempFile + Integer.toString(rank))) {
				 movesToCheck.add((char)tempFile + Integer.toString(rank));
				 break;
			 } else {
				 movesToCheck.add((char)tempFile + Integer.toString(rank));
			 }
			 tempFile++;
		 }
		 for(int i =0; i < movesToCheck.size(); i ++) {
			 checkAndAdd(validMoves, movesToCheck.get(i), color);
		 }
	 }
	 
	 /**
	  * Adds all of the validMoves for a Bishop to the validMoves Set.
	 * @param validMoves the set where valid moves will be added to
	 * @param color current color
	 * @param file current file
	 * @param rank current rank
	 */
	private static void getBishopMoves(Set<String> validMoves, char color, char file, int rank) {
		 List<String> movesToCheck = new ArrayList<String>();
		 int tempRank = rank;
		 char tempFile = file;
		 tempRank--;
		 tempFile--;
		 while(tempRank > 0 && tempFile >= 'a') {
			 if(board.containsKey((char)tempFile + Integer.toString(tempRank))) {
				 movesToCheck.add((char)tempFile + Integer.toString(tempRank));
				 break;
			 } else {
				 movesToCheck.add((char)tempFile + Integer.toString(tempRank));
			 }
			 tempRank--;
			 tempFile--;
		 }
		 tempRank = rank;
		 tempFile = file;
		 tempRank++;
		 tempFile++;
		 while(tempRank < 9 && tempFile <= 'h') {
			 if(board.containsKey((char)tempFile + Integer.toString(tempRank))) {
				 movesToCheck.add((char)tempFile + Integer.toString(tempRank));
				 break;
			 } else {
				 movesToCheck.add((char)tempFile + Integer.toString(tempRank));
			 }
			 tempRank++;
			 tempFile++;
		 }
		 tempFile = file;
		 tempRank = rank;
		 tempFile--;
		 tempRank++;
		 while(tempFile >= 'a' && tempRank < 9) {
			 if(board.containsKey((char)tempFile + Integer.toString(tempRank))) {
				 movesToCheck.add((char)tempFile + Integer.toString(tempRank));
				 break;
			 } else {
				 movesToCheck.add((char)tempFile + Integer.toString(tempRank));
			 }
			 tempFile--;
			 tempRank++;
		 }
		 tempFile = file;
		 tempRank = rank;
		 tempFile++;
		 tempRank--;
		 while(tempFile <= 'h' && tempRank > 0) {
			 if(board.containsKey((char)tempFile + Integer.toString(tempRank))) {
				 movesToCheck.add((char)tempFile + Integer.toString(tempRank));
				 break;
			 } else {
				 movesToCheck.add((char)tempFile + Integer.toString(tempRank));
			 }
			 tempFile++;
			 tempRank--;
		 }
		 for(int i =0; i < movesToCheck.size(); i ++) {
			 checkAndAdd(validMoves, movesToCheck.get(i), color);
		 }
		 
	 }
	 /** Adds all of the valid moves for the King peice into the Set validMoves
	 * @param validMoves the set containing all of the valid moves 
	 * @param color current color piece
	 * @param file current file
	 * @param rank current rank
	 */
	private static void getKingMoves(Set<String> validMoves, char color, char file, int rank) {
		 List<String> movesToCheck = new ArrayList<String>();
		 
		 if(rank - 1 > 0) movesToCheck.add((char) (file) + Integer.toString(rank-1));
		 if(rank + 1 < 9) movesToCheck.add((char) (file) + Integer.toString(rank+1));
		 if(file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank));
		 if(file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank));
		 if(rank + 1 < 9 && file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank+1));
		 if(rank - 1 > 0 && file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank-1));
		 if(rank + 1 < 9 && file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank+1));
		 if(rank - 1 > 0 && file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank-1));
		 //now check if these moves are valid
		 for(int i =0; i < movesToCheck.size(); i ++) {
			 
			 checkAndAdd(validMoves, movesToCheck.get(i), color);
		 }
		 
		 
		 }
		 
	 
	 
	 public static void debug() throws IOException {
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 printBoard();
		 boolean gameOver = false;
		 while(gameOver == false) {
			 
			 String lastMove =playTurn("w", br);
			 if(lastMove.equals(blackKingLocation))gameOver = true;
			 printBoard();
		 }
		 System.out.println("White wins");
	 }
	 
	 
	 /**
	  * gathers all potential moves of the player
	 * @param boardKeys - the keySet of board
	 * @param color - the color of the player which moves the user wants to obtain
	 * @return a set of Strings containing all potential moves
	 */
	public static Set<String> getAllPlayerMoves(Set<String> boardKeys, char color){
		 
		 Set<String> validMoves = new HashSet<>();
		 for(String cur : boardKeys) {
			 ChessPiece chessPiece = board.get(cur);
			 if(chessPiece.toString().charAt(0) == color) {
			 char piece = chessPiece.toString().charAt(1);
			 char file = cur.charAt(0);
			 int rank = Integer.parseInt(cur.substring(1));
			 if(piece == 'P') {
				 getPawnMoves(validMoves, color, file, rank);
			 } else if(piece == 'N') {
				 getKnightMoves(validMoves, color, file, rank);
			 } else if(piece == 'R') {
				 getRookMoves(validMoves, color, file, rank);
			 } else if(piece == 'B') {
				 getBishopMoves(validMoves, color, file, rank);
			 } else if(piece == 'Q') { // since queen has the same moves as Bishop + Rook
				 getBishopMoves(validMoves, color, file, rank);
				 getRookMoves(validMoves, color, file, rank);
			 } else {
				 getKingMoves(validMoves, color, file, rank);
			 }
			 }
		 }
		 return validMoves;
	 }
	 
	 /**
	  * Checks if castling can occur
	 * @param cur current location of the King
	 * @param dest the destination of the King
	 * @param color the color of the current player
	 * @return boolean true if it can false otherwise
	 */
	public static boolean checkCastle(String cur, String dest, char color) {
		 
		 if(!check) {
			 if(color == 'w') {
				 if(!whiteKingMoved) {
					 Set<String> opponentMoves = getAllPlayerMoves(board.keySet(), 'b');
					 if(dest.equals("g1")) {
					 if(!board.containsKey("f1") && !board.containsKey("g1") && !rightWhiteRookMoved && !opponentMoves.contains("f1") && !opponentMoves.contains("g1")) {
						 board.put("f1", board.get("h1"));
						 board.put("g1", board.get("e1"));
						 board.remove("h1");
						 board.remove("e1");
						 whiteKingMoved = true;
						 rightWhiteRookMoved = true;
						 whiteKingLocation = "g1";
						 return true;
					 } 
					 } else {
						 if(!board.containsKey("d1") && !board.containsKey("c1") && !leftWhiteRookMoved && !opponentMoves.contains("c1") && !opponentMoves.contains("d1")) {
							 board.put("d1", board.get("a1"));
							 board.put("c1", board.get("e1"));
							 board.remove("a1");
							 board.remove("e1");
							 whiteKingMoved = true;
							 leftWhiteRookMoved = true;
							 whiteKingLocation = "c1";
							 return true;
						 }
					 }
					 
				 }
			 } else {
				 if(!blackKingMoved) {
					 Set<String> opponentMoves = getAllPlayerMoves(board.keySet(), 'w');
					 if(dest.equals("g8")) {
					 if(!board.containsKey("f8") && !board.containsKey("g8") && !rightBlackRookMoved && !opponentMoves.contains("f8") && !opponentMoves.contains("g8")) {
						 board.put("f8", board.get("h8"));
						 board.put("g8", board.get("e8"));
						 board.remove("h8");
						 board.remove("e8");
						 blackKingMoved = true;
						 rightBlackRookMoved = true;
						 blackKingLocation = "g8";
						 return true;
					 } 
					 } else {
						 if(!board.containsKey("d8") && !board.containsKey("c8") && !leftBlackRookMoved && !opponentMoves.contains("c8") && !opponentMoves.contains("d8")) {
							 board.put("d8", board.get("a8"));
							 board.put("c8", board.get("e8"));
							 board.remove("a8");
							 board.remove("e8");
							 blackKingMoved = true;
							 leftBlackRookMoved = true;
							 blackKingLocation = "c8";
							 return true;
						 }
					 }
					 
				 }
			 }
		 }
		 return false;
		 
	 }
}