package chesspieces;

/**
 * @author Matthew Skrobola, Kyle Reagle
 * <p>
 * The ChessPiece class is the super class of all the chess piece type's subclasses
 */
public class ChessPiece {
	
	/**
	 * The color of the chess piece, represented as "b" or "w"
	 */
	private String color;
	/**
	 * The location of the chess piece, represented as a two character string
	 * where the file is the first character and the rank is the second character
	 */
	private String location;
	
	/**
	 * @param color color of chess piece
	 * @param location location of chess piece
	 */
	public ChessPiece(String color,String location) {
		this.color = color;
		this.location = location;
	}

	public void move(String newLocation) {
		location = newLocation;
	}
	
	/**
	 * @return the color of the piece. Either white or black.
	 */
	public String getColor() {
		return color;
	}
	
	/**
	 * @return the location of the piece on the board.
	 */
	public String getLocation() {
		return location;
	}
	
	/**
	 * sets the location for the piece.
	 * @param location the updated location.
	 */
	public void setLocation(String location) {
		this.location = location;
	}

}
