package chesspieces;

/**
 * @author Matthew Skrobola, Kyle Reagle
 *
 */
public class Knight extends ChessPiece {
	
	/** 
	 * Knight chess piece
	 * @param color color of the knight
	 * @param location location of the knight
	 */
	public Knight(String color, String location) {
		super(color,location);
	}
	
	/** 
	 * @return the correct notation for printing to the board
	 */
	public String toString() {
		return getColor()+"N";
	}
}
