# README #

A simple game of chess built in Java 8

- Everything seems to working fine as far as the main part of the project goes. I think the logic for stalemate is correct, it's just that getAllPlayerMoves doesn't return a list of truly vaild moves. This can probably be fixed by testing all moves in that list to see if it puts the player into checkmate if it were to be played


Pseudocode for stalemate:

 // this would be placed at line 184
if (check == false) {
	if (whitesTurn) {
		moves = getAllPlayerMoves(board.keySet(), 'w');
	} else {
		moves = getAllPlayerMoves(board.keySet(), 'b');
	}

	int checkMateCount = 0;

	Map<String,ChessPiece> tempBoard;

	for (String move: moves) {
		tempBoard = board;
		// update tempBoard with move
		// check if tempBoard has checkmate
		// if checkmate, increment checkMateCount
	}
	
	if (checkMateCount == moves.size()) {
		System.out.println("stalemate");
		break;
	}

}

Test moves for stalemate(this can be pasted in to the console all at once):
c2 c4
h7 h5
h2 h4
a7 a5
d1 a1
d1 a4
a8 a6
a4 a5
a6 h6
a5 c7
f7 f6
c7 d7
e8 f7
d7 b7
d8 d3
b7 b8
d3 h7
b8 c8
f7 g6
c8 e6